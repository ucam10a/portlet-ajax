<%--

    Licensed to Jasig under one or more contributor license
    agreements. See the NOTICE file distributed with this work
    for additional information regarding copyright ownership.
    Jasig licenses this file to you under the Apache License,
    Version 2.0 (the "License"); you may not use this file
    except in compliance with the License. You may obtain a
    copy of the License at:

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on
    an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied. See the License for the
    specific language governing permissions and limitations
    under the License.

--%>

<jsp:directive.include file="/WEB-INF/jsp/include.jsp"/>

<portlet:resourceURL var="getJsonURL" id="ajaxHandler"/>

<script src="<rs:resourceURL value="/rs/jquery/1.8.3/jquery-1.8.3.min.js"/>" type="text/javascript"></script>
<c:set var="n"><portlet:namespace/></c:set>
<!-- ajax JS code -->
<script type="text/javascript">
    var ${n} = {};
    // declare local jQuery
    ${n}.jQuery = jQuery.noConflict(true);
    // declare local ajax http send method
    ${n}.ajaxHttpSend = function ajaxHttpSend(action, form, response){
        // use local jquery
        var $ = ${n}.jQuery;
        // url
        var ajaxurl = action;
        //alert(ajaxurl + " ok");
    
        // param serialize
        var params = $('#' + form).serialize();
        //alert(params);
        
        $.ajax({
            type:'POST',
            // to make sure we get string text
            dataType: 'text',
            error: function(x, status, error) { 
                alert("An ajax error occurred: " + status + " Error: " + error);
            },
            url:ajaxurl,
            data:params,
            async: false,
            success:function(result){
                response(result);
            }
        });
    };
</script>

<h2><spring:message code="portlet.hello"/> ${ fn:escapeXml(displayName) }!</h2>

<p><spring:message code="portlet.emailAddress"/> ${ fn:escapeXml(emailAddress) }</p>


<!-- ajax form -->
<form id="${n}AjaxTestForm" name="${n}AjaxTestForm" method="post">
    <input name="firstName" type="text" value="" />
    <input name="lastName" type="text" value="" />
    <input id="${n}AjaxTestCall" type="button" value="submit" onClick='${n}.ajaxHttpSend("${getJsonURL}", "${n}AjaxTestForm", ${n}.callBack);'/>
</form>


<!-- ajax callback -->
<script>
    ${n}.callBack = function callBack(responseText){
        // make sure you use local jQuery
        var res = ${n}.jQuery.parseJSON(responseText);
        alert(res.message);
        alert(res.firstName);
        alert(res.lastName);
    };
</script>
